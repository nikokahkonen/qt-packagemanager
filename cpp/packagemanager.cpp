/*
Copyright (c) 2014 Niko Kähkönen <niko.v.kahkonen@gmail.com>

This file is part of qt-packagemanager.

qt-packagemanager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QDebug>
#include "packagemanager.h"
#include "packagemanagerfactory.h"


QSharedPointer<PackageManager> PackageManager::create(const QString &manager)
{
    QSharedPointer<PackageManager> mgr = PackageManagerFactory::createManager(manager);
    mgr->packages(); // loads package list
    return mgr;
}


PackageManager::PackageManager(QObject *parent) :
    QProcess(parent)
{
    setProcessChannelMode(MergedChannels);
}


const QList<Package>& PackageManager::packages()
{
    start(listPackagesCommand());
    waitForFinished();
    QString output = readAll();
    _packages = parseListPackages(output);
    return _packages;
}


int PackageManager::packageCount() const
{
    // todo: how to make sure the package list is always up-to-date ?
    int count = _packages.count();
    return count;
}


Package& PackageManager::package(int index)
{
    if(index < _packages.count())
        return _packages[index];

    static Package empty;
    return empty;
}


QString PackageManager::version()
{
    start(versionCommand());
    waitForFinished();
    QString output = readAll();
    return parseVersion(output);
}
