/*
Copyright (c) 2014 Niko Kähkönen <niko.v.kahkonen@gmail.com>

This file is part of qt-packagemanager.

qt-packagemanager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PACKAGELISTMODEL_H
#define PACKAGELISTMODEL_H


#include <QAbstractListModel>
#include <QSharedPointer>
#include "packagemanager.h"


///
/// \brief This class is the main data model containing installed software packages.
///        Can be used with QListView or QML ListView components.
///
class PackageListModel : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(QString packageManagerName READ packageManagerName WRITE setPackageManager)

public:
    ///
    /// \brief The PackageItemRoles defines available data types of single item
    ///
    enum PackageItemRoles {
        StatusRole = Qt::UserRole + 1,  ///< package status
        NameRole,                       ///< package name
        VersionRole                     ///< package version
    };

    explicit PackageListModel(QObject *parent = 0);

    // QAbstractListModel overridables
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

protected:
    QHash<int, QByteArray> roleNames() const;

public slots:
    ///
    /// \brief Returns the current package manager name as string
    /// \return Package manager name string, for example "dpkg" or "rpm"
    ///
    QString packageManagerName() const;

    ///
    /// \brief Sets current package manager
    /// \param packageManagerName Package manager name string, for example "dpkg" or "rpm"
    /// \return True if succeeded, false if given manager cannot be used
    ///
    bool setPackageManager(QString packageManagerName);

    ///
    /// \brief Reads all packages from the system using given package manager and updates the model
    /// \param packageManagerName The package manager to be used
    /// \return True if succeeded, false if given manager cannot be used
    ///
    bool update(QString packageManagerName);

private:
    QString _packageManagerName; ///< package manager to be used with this model
    QSharedPointer<PackageManager> _packageManager; ///< current package manager instance
};

#endif // PACKAGELISTMODEL_H
