/*
Copyright (c) 2014 Niko Kähkönen <niko.v.kahkonen@gmail.com>

This file is part of qt-packagemanager.

qt-packagemanager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PACKAGE_H
#define PACKAGE_H

#include <QObject>
#include <QString>
#include <QStringList>
#include <QSharedPointer>
#include <QSharedData>
#include <QSharedDataPointer>


class PackageManager;

///
/// \brief This class holds generic information of single software package
///
/// \todo REFACTOR THIS CLASS!!!!!!!!
///       1) Move dpkg specific functionality to derived PackageDpkg class
///       2) Change PackageManagerDpkg to use PackageDpkg and so on
///       3) Do the code cleanup for private part
///
class Package
{
public:
    ///
    /// \brief This enumerator describes the status of the package found from system
    ///
    enum Status {
        Undefined = -1,
        Unknown,                    // 'u' in dpkg packages
        Installed,                  // 'ii'     -"-
        ConfigFilesInstalled        // 'rc'     -"-
    };

public:
    ///
    /// \brief Standard constructor
    ///
    Package();

    ///
    /// \brief Constructor for dpkg packages
    /// \todo: Refactor: Create derived DpkgPackage class and move this there!
    /// \param dpkgLine Single output line from dpkg command line output
    ///
    Package(QString dpkgLine);

    ///
    /// \brief Copy constructor
    /// \param other Another package instance to be copied
    ///
    Package(const Package& other);

    ///
    /// \brief Returns true if this package instance in uninitialized
    /// \return true if uninitialized, false if initialized
    ///
    bool isNull() const;

    ///
    /// \brief Returns the package name
    /// \return The package name as string
    ///
    QString name() const;

    ///
    /// \brief Returns the package version as string. This can be displayed as is to user
    /// \return The version number information as string
    ///
    QString version() const;

    ///
    /// \brief Returns package status
    /// \return The package status
    /// \sa enum Status
    ///
    Status status() const;

private:
    Status statusFromString(QString const& str) const;
    bool startProcess(QString cmd, QStringList args);
    bool fromDpkgOutputLine(QString line);

private:
    class PackagePrivate : public QSharedData
    {
    public:
        PackagePrivate(Status status = Undefined,
                       QString name = QString(),
                       QString version = QString(),
                       QString description = QString(),
                       QSharedPointer<PackageManager> manager = QSharedPointer<PackageManager>())
                     : _status(status),
                       _name(name),
                       _version(version),
                       _description(description),
                       _manager(manager)
        {}

        PackagePrivate(const PackagePrivate &other)
            : QSharedData(other),
              _status(other._status),
              _name(other._name),
              _version(other._version),
              _description(other._description),
              _manager(other._manager)
        {}

        Status status() const { return _status; }
        QString name() const { return _name; }
        QString version() const { return _version; }
        QString description() const { return _description; }
        QSharedPointer<PackageManager> const& manager() const { return _manager; }

    private:
        Status _status;
        QString _name;
        QString _version;
        QString _description;
        QSharedPointer<PackageManager> _manager;
    };

    QSharedDataPointer<PackagePrivate> _data;
};

#endif // PACKAGE_H
