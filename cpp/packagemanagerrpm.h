/*
Copyright (c) 2014 Niko Kähkönen <niko.v.kahkonen@gmail.com>

This file is part of qt-packagemanager.

qt-packagemanager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef PACKAGEMANAGERRPM_H
#define PACKAGEMANAGERRPM_H

#include "packagemanager.h"

///
/// \brief Package manager class for rpm packages
/// \sa PackageManager
///
class PackageManagerRpm : public PackageManager
{
    Q_OBJECT
public:
    explicit PackageManagerRpm(QObject *parent = 0);

signals:

public slots:
    QString name() const;

    bool isInstalled();

    bool installPackage(QString const& name);

    bool uninstallPackage(QString const& name);

    QStringList installedPackages();

protected:
    virtual QString versionCommand() const;
    virtual QString parseVersion(QString input);

    virtual QString listPackagesCommand();
    virtual QList<Package> parseListPackages(QString input);
};

#endif // PACKAGEMANAGERRPM_H
