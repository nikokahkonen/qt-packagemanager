/*
Copyright (c) 2014 Niko Kähkönen <niko.v.kahkonen@gmail.com>

This file is part of qt-packagemanager.

qt-packagemanager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <QDebug>
#include "packagemanagerfactory.h"
#include "packagemanager.h"
#include "packagemanagerdpkg.h"
#include "packagemanagerrpm.h"


PackageManagerFactory::PackageManagerFactory()
{
}


PackageManagerFactory::~PackageManagerFactory()
{
}


QStringList PackageManagerFactory::managers()
{
    QStringList ret;
    ret << PackageManagerDpkg().name();
    ret << PackageManagerRpm().name();
    return ret;
}


QStringList PackageManagerFactory::advancedManagers()
{
    return QStringList();
}


QSharedPointer<PackageManager> PackageManagerFactory::createManager(const QString manager)
{
    if(manager == PackageManagerDpkg().name())
        return QSharedPointer<PackageManager>(new PackageManagerDpkg());

    if(manager == PackageManagerRpm().name())
        return QSharedPointer<PackageManager>(new PackageManagerRpm());

    qWarning() << Q_FUNC_INFO << "Package manager" << manager << "not supported!";
    return QSharedPointer<PackageManager>();
}
