/*
Copyright (c) 2014 Niko Kähkönen <niko.v.kahkonen@gmail.com>

This file is part of qt-packagemanager.

qt-packagemanager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QDebug>
#include "packagelistmodel.h"
#include "packagemanager.h"



PackageListModel::PackageListModel(QObject *parent) :
    QAbstractListModel(parent),
    _packageManager()
{
}


int PackageListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    if(!_packageManager) {
        qWarning() << "  invalid package manager";
        return 0;
    }

    return _packageManager->packageCount();
}


QVariant PackageListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (!_packageManager)
        return QVariant();

    if (index.row() < 0 || index.row() > _packageManager->packageCount())
        return QVariant();


    switch (role)
    {
        case Qt::DisplayRole:
            return QVariant(_packageManager->installedPackages().at(index.row()));
        case StatusRole:
            return QVariant(_packageManager->package(index.row()).status());
        case NameRole:
            return QVariant(_packageManager->package(index.row()).name());
        case VersionRole:
            return QVariant(_packageManager->package(index.row()).version());
        default:
            qWarning() << "PackageListModel::data(): Unsupported data role: " << role;
            return QVariant();
    }
}


QHash<int, QByteArray> PackageListModel::roleNames() const
{
    qDebug() << Q_FUNC_INFO;
    QHash<int, QByteArray> roles;
    roles[StatusRole] = "packageStatus";
    roles[NameRole] = "packageName";
    roles[VersionRole] = "packageVersion";
    qDebug() << roles;
    return roles;
}


QString PackageListModel::packageManagerName() const
{
    if (!_packageManager)
        return QString();

    return _packageManager->name();
}


bool PackageListModel::setPackageManager(QString packageManagerName)
{
    qDebug() << Q_FUNC_INFO << packageManagerName;

    beginResetModel();

    QSharedPointer<PackageManager> manager = PackageManager::create(packageManagerName);
    if (!manager) {
        return false;
    }

    _packageManager = manager;
    _packageManager->packages();

    endResetModel();

    return true;
}


bool PackageListModel::update(QString packageManagerName)
{
    qDebug() << Q_FUNC_INFO << packageManagerName;
    return setPackageManager(packageManagerName);
}
