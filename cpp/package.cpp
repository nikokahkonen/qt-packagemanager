/*
Copyright (c) 2014 Niko Kähkönen <niko.v.kahkonen@gmail.com>

This file is part of qt-packagemanager.

qt-packagemanager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QProcess>
#include <QStringList>
#include "package.h"
#include "packagemanager.h"


Package::Package()
    : _data(new PackagePrivate())
{
}


Package::Package(QString dpkgLine)
    : _data (new PackagePrivate())
{
    fromDpkgOutputLine(dpkgLine);
}


Package::Package(const Package &other)
    : _data (other._data)
{
}


bool Package::isNull() const
{
    if (!_data)
        return true;

    if (_data->status() == Undefined &&
        _data->name().isEmpty()      &&
        _data->version().isEmpty())
    {
        return true;
    }
    return false;
}


QString Package::name() const
{
    if (_data) {
        return _data->name();
    }

    return QString();
}


QString Package::version() const
{
    if (_data) {
        return _data->version();
    }

    return QString();
}


Package::Status Package::status() const
{
    if (_data) {
        return _data->status();
    }

    return Undefined;
}


// todo: this is dpkg specific functionality -> move to new class: DpkgPackage
bool Package::fromDpkgOutputLine(QString line)
{
    if(_data)
        _data.detach();

    QStringList data = line.split(' ', QString::SkipEmptyParts);
    if( data.size() < 4)
        return false;

    _data = QSharedDataPointer<PackagePrivate>(
                new PackagePrivate (statusFromString(data[0]),
                                    data[1],
                                    data[2],
                                    data[3])
            );
    return true;
}


Package::Status Package::statusFromString(QString const& str) const
{
    if( str == "ii") return Installed;
    if( str == "rc") return ConfigFilesInstalled;
    // todo: add rest of combinations
    // todo2: this isdpkg specific functionality -> move to new class: DpkgPackage
    return Unknown;
}
