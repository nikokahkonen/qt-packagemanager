/*
Copyright (c) 2014 Niko Kähkönen <niko.v.kahkonen@gmail.com>

This file is part of qt-packagemanager.

qt-packagemanager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "packagemanagerdpkg.h"
#include <QDebug>
#include <QTime>


namespace
{
static const QString SHELL("sh");
static const QString DPKG("dpkg");
}



PackageManagerDpkg::PackageManagerDpkg(QObject *parent) :
    PackageManager(parent)
{
}


QString PackageManagerDpkg::name() const
{
    return "dpkg";
}


bool PackageManagerDpkg::isInstalled()
{
    start(DPKG);
    return waitForFinished(10000);
}


bool PackageManagerDpkg::installPackage(QString const& name)
{
    start(DPKG, QStringList() << "-i" << name);
    if( !waitForFinished()) {
        // failed to start process
        return false;
    }
    // todo: examine return value
    return true;
}


bool PackageManagerDpkg::uninstallPackage(QString const& name)
{
    start( DPKG, QStringList() << "-r" << name);
    waitForFinished();
    // todo: examine return value
    return true;
}


QStringList PackageManagerDpkg::installedPackages()
{
    start(DPKG, QStringList() << "-l");

    if( !waitForFinished(10000))
        return QStringList();

    return QString(readAll()).split('\n', QString::SkipEmptyParts);
}


QString PackageManagerDpkg::versionCommand() const
{
    return "dpkg --version";
}


QString PackageManagerDpkg::parseVersion(QString input)
{
    /*
    The version text returned by dpkg is something like:

    Debian `dpkg' package management program version 1.16.1.2 (i386).
    This is free software; see the GNU General Public License version 2 or
    later for copying conditions. There is NO warranty.
    */

    QStringList words = input.split(' ', QString::SkipEmptyParts);
    if(words.count() > 7)
        return words[7];

    /*
    // todo: implement regexp check for more reliable functionality
    QRegularExpression re("...");
    foreach(QString word, words) {
        ...
    }
    */

    return "n/a";
}


QString PackageManagerDpkg::listPackagesCommand()
{
    return QString("dpkg -l");
}


QList<Package> PackageManagerDpkg::parseListPackages(QString input)
{
    QList<Package> packages;

    // split into rows
    QStringList rows = input.split('\n');
    foreach(const QString row, rows) {
        Package pkg(row);
        if (pkg.status() != Package::Unknown &&
            pkg.status() != Package::Undefined)
        {
            packages.push_back(pkg);
        }
    }

    return packages;
}
