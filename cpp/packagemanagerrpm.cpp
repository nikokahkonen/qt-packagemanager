/*
Copyright (c) 2014 Niko Kähkönen <niko.v.kahkonen@gmail.com>

This file is part of qt-packagemanager.

qt-packagemanager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "packagemanagerrpm.h"

// binary name
static const QString RPM("rpm");


PackageManagerRpm::PackageManagerRpm(QObject *parent) :
    PackageManager(parent)
{
}


QString PackageManagerRpm::name() const
{
    return "rpm";
}


bool PackageManagerRpm::isInstalled()
{
    start(RPM);
    return waitForFinished();
}


bool PackageManagerRpm::installPackage(const QString &name)
{
    Q_UNUSED(name);
    // not yet implemented
    return false;
}


bool PackageManagerRpm::uninstallPackage(const QString &name)
{
    Q_UNUSED(name);
    // not yet implemented
    return false;
}


QStringList PackageManagerRpm::installedPackages()
{
    start(RPM, QStringList() << "-qa");
    waitForFinished();
    QString output = readAll();
    return output.split('\n', QString::SkipEmptyParts);
}


QString PackageManagerRpm::versionCommand() const
{
    return "rpm --version";
}


QString PackageManagerRpm::parseVersion(QString input)
{
    // todo: implement this properly
    return input;
}


QString PackageManagerRpm::listPackagesCommand()
{
    return QString("rpm -qa");
}


QList<Package> PackageManagerRpm::parseListPackages(QString input)
{
    Q_UNUSED(input);
    return QList<Package>();
}
