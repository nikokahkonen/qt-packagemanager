/*
Copyright (c) 2014 Niko Kähkönen <niko.v.kahkonen@gmail.com>

This file is part of qt-packagemanager.

qt-packagemanager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef PACKAGEMANAGERFACTORY_H
#define PACKAGEMANAGERFACTORY_H

#include <QStringList>
#include <QSharedPointer>

class PackageManager;

///
/// \brief This class is used to manage and create package managers
/// of different types
///
/// \example
/// \code
/// QMap< QString, QSharedPointer<PackageManager > > managers;
/// QStringList managers = PackageManagerFactory::managers();
/// foreach(QString manager, managers) {
///     managers[manager] = PackageManagerFactory::createManager(manager);
/// }
/// \endcode
///
class PackageManagerFactory
{
public:
    ///
    /// \brief Returns the list of supported package managers like dpkg or rpm
    /// \return The string list of package manager identifiers
    ///
    static QStringList managers();

    ///
    /// \brief Returns the list of supported advanced package managers like apt-get, aptitude or yum
    /// \return The string list of package manager identifiers
    ///
    static QStringList advancedManagers();

    ///
    /// \brief Creates he package manager of given type
    /// \param manager The package manager string identifier
    /// \return The shared pointer to created package manager object
    ///
    static QSharedPointer<PackageManager> createManager(const QString manager);

private:
    /// static class, constructor disabled
    PackageManagerFactory();
    ~PackageManagerFactory();

    Q_DISABLE_COPY(PackageManagerFactory);
};

#endif // PACKAGEMANAGERFACTORY_H
