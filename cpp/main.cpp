/*
Copyright (c) 2014 Niko Kähkönen <niko.v.kahkonen@gmail.com>

This file is part of qt-packagemanager.

qt-packagemanager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QApplication>
#include <QQuickView>

#include "packagelistmodel.h"
#include "package.h"


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    qmlRegisterType<PackageListModel>("QtPackageManager", 1, 0, "PackageListModel");

    QQuickView mainWindow;
    mainWindow.setResizeMode(QQuickView::SizeRootObjectToView);
    mainWindow.setSource(QUrl("qrc:///qml/qml/MainWindow.qml"));
    mainWindow.show();

    return a.exec();
}
