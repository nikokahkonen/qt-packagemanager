/*
Copyright (c) 2014 Niko Kähkönen <niko.v.kahkonen@gmail.com>

This file is part of qt-packagemanager.

qt-packagemanager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PACKAGEMANAGER_H_INCLUDED
#define PACKAGEMANAGER_H_INCLUDED

#include <QProcess>
#include <QList>

#include "package.h"


class PackageManager : public QProcess
{
    Q_OBJECT

public:
    ///
    /// \brief The factory function. Creates package manager of given type
    /// \param manager The package manager type
    /// \return Shared pointer to new package manager object
    ///
    static QSharedPointer<PackageManager> create(const QString& manager);

    ///
    /// \brief Constructor
    /// \param parent The parent object
    ///
    explicit PackageManager(QObject *parent = 0);

    ///
    /// \brief Destructor
    ///
    virtual ~PackageManager() {}

    ///
    /// \brief Returns the list of all known packages
    /// \return The list of
    ///
    const QList<Package>& packages();

    ///
    /// \brief Returns the count of packages in system
    /// \return The package count
    ///
    int packageCount() const;

    ///
    /// \brief Returns reference to package if given index
    /// \param index Zero based index of wanted package. Must be smaller than package count
    /// \return Package info as string
    ///
    Package& package(int index);

public slots:
    ///
    /// \brief Returns unique package manager name: "dpkg" for dpkg and "rpm" for rpm etc.
    ///        This is needed by PackageManagerFactory class
    /// \return Package manager name
    ///
    virtual QString name() const = 0;

    ///
    /// \brief Indicates weather the package manager is installed or not.
    /// \return true if installed, false if not installed.
    ///
    virtual bool isInstalled() = 0;

    ///
    /// \brief Returns the package manager version number
    /// \return The package manager version as string.
    ///
    QString version();

    ///
    /// \brief Installs given package.
    /// \param name The package name or file name
    /// \return true if succeeded, false if failed
    ///
    virtual bool installPackage(QString const& name) = 0;

    ///
    /// \brief Removes given package, if installed.
    /// \param name The package name
    /// \return true if succeeded, false if failed
    ///
    virtual bool uninstallPackage(QString const& name) = 0;

    ///
    /// \brief Gives list of packages installed by this package manager.
    /// \return  List of installed package names.
    ///
    virtual QStringList installedPackages() = 0;

protected:
    ///
    /// \brief Implement this to return the version command
    /// \return The command to query the version info from the manager process
    ///
    virtual QString versionCommand() const = 0;

    ///
    /// \brief Implement this to handle the version command
    /// \return The version number as sring, parsed from input string
    ///
    virtual QString parseVersion(QString input) = 0;

    ///
    /// \brief Implement this in subclass to return list packages command
    /// \return The command to query the list of packages in system
    ///
    virtual QString listPackagesCommand() = 0;

    ///
    /// \brief Implement this in subclass to parse list of packages
    /// \param The input string (output from package manager command line tool)
    /// \return The command to query the list of packages in system
    ///
    virtual QList<Package> parseListPackages(QString input) = 0;

private:
    QList<Package> _packages; ///< package cache
    Q_DISABLE_COPY(PackageManager)
};

#endif // PACKAGEMANAGER_H
