qt-packagemanager  
=================  

Qt Package Manager is simple, fast, easy-to-use multi-packet manager gui
for all most common Linux package managers: dpkg, rpm, apt-get, yum, pip, npm...

The idea of this project is to utilize Qt QProcess to use external package managers
through their command line interface. This is how we minimize our dependencies to all
external package manager API modules, still being able to build kind of plug-and-play
support for any package manager user has installed on the system.
This approach also allows us to support all possible package managers, if they only can
be used from command line.

The home of this project is in BitBucket:
https://bitbucket.org/nikokahkonen/qt-packagemanager


Build and installation  
----------------------  
qmake  
make  
sudo make install  


Roadmap / short term feature plan  
---------------------------------  

[X]  Basic UI for dpkg packages (just shows packages from system)  
[  ]  Package search/filter: free text search by package name  
[  ]  UI shows detailed package information for selected dpkg package  
[  ]  UI supports several package managers (dpkg and rpm)  
[  ]  UI for rpm packages with real rpm support (list packages and package details)  
[  ]  Uninstall support for installed dpkg and rpm packages  
[  ]  Install support for dpkg and rpm files  
[  ]  Support for some advanced package manager with centralized repo (apt-get/yum)  
[  ]  Support for some other package manager (pip/npm)  
