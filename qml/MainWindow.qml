import QtQuick 2.0
import QtPackageManager 1.0


Rectangle {
    width: 640
    height: 480

    PackageListModel {
        id : packageListModel
    }

    ListView {
        Component.onCompleted: {
            packageListModel.update("dpkg")
        }

        anchors.fill : parent
        model : packageListModel

        delegate: Rectangle {

            id : delegateItem
            width : parent.width
            height : packageNameText.height

            property int default_margin : 6

            Image {
                id : icon

                height : parent.height - default_margin
                width : height

                source : {
                    switch(packageStatus)
                    {
                    case -1:// Undefined
                        return "qrc:/icons/error"
                    case 0: // Unknown
                        return "qrc:/icons/error"
                    case 1: // Installed
                        return "qrc:/icons/ok"
                    case 2: // ConfigFilesInstalled
                        return "qrc:/icons/settings"
                    default: // unknown situation
                        return "qrc:/icons/error"
                    }

                    /*
                      enum values from Package.h:
                      (todo: supply these better to qml side)

                      Undefined = -1,
                      Unknown   =  0,                    // 'u' in dpkg packages
                      Installed =  1                     // 'ii'     -"-
                      ConfigFilesInstalled = 2           // 'rc'     -"-

                    */
                }

                anchors.left : parent.left
                anchors.verticalCenter: parent.verticalCenter

                anchors.leftMargin : default_margin
            }

            Text {
                id : packageNameText

                anchors.left : icon.right
                anchors.right : parent.right
                anchors.verticalCenter : parent.verticalCenter

                anchors.leftMargin : default_margin

                text : packageName
            }
        }
    }
}
