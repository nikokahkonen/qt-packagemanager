#-------------------------------------------------
#
# Project created by QtCreator 2014-01-12T01:38:25
#
#-------------------------------------------------

QT       += core gui quick

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qt-packagemanager
TEMPLATE = app

INCLUDEPATH += cpp


SOURCES += cpp/packagelistmodel.cpp \
    cpp/package.cpp \
    cpp/packagemanagerdpkg.cpp \
    cpp/packagemanager.cpp \
    cpp/packagemanagerfactory.cpp \
    cpp/packagemanagerrpm.cpp


HEADERS  += cpp/packagelistmodel.h \
    cpp/package.h \
    cpp/packagemanagerdpkg.h \
    cpp/packagemanager.h \
    cpp/packagemanagerfactory.h \
    cpp/packagemanagerrpm.h


# for test suite
test{
    TARGET = test_qt-packagemanager # different app name for testing suite

    QT += testlib

    HEADERS += tests/test_packagemanagerdpkg.h \

    SOURCES += tests/test_packagemanagerdpkg.cpp \
        tests/test_main.cpp

} else {
    SOURCES += cpp/main.cpp
}

RESOURCES += \
    qt-packagemanager.qrc \

OTHER_FILES += README.md \

