/*
Copyright (c) 2014 Niko Kähkönen <niko.v.kahkonen@gmail.com>

This file is part of qt-packagemanager.

qt-packagemanager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QObject>
#include <QtTest/QtTest>



void test_PackageManagerProxy::initTestCase()
{
}


void test_PackageManagerProxy::cleanupTestCase()
{
}


void test_PackageManagerProxy::packageManagers()
{
    QStringList managers;
    QBENCHMARK {
        managers = _proxy.packageManagers();
    }
    QVERIFY(!managers.isEmpty());
}


void test_PackageManagerProxy::installedPackagesFirstTime()
{
    QStringList packages;
    QBENCHMARK_ONCE {
        packages = _proxy.installedPackages();
    }
    QVERIFY(!packages.isEmpty());
}


void test_PackageManagerProxy::installedPackagesFromCache()
{
    QStringList packages;
    QBENCHMARK {
        packages = _proxy.installedPackages();
    }
    QVERIFY(!packages.isEmpty());
}


void test_PackageManagerProxy::installedPackages()
{
    // from all available managers
    QStringList packages;
    QBENCHMARK {
        packages = _proxy.installedPackages();
    }
    QVERIFY(!packages.isEmpty());
    int allCount = packages.count();

    // check all package managers also separately
    QStringList managers = _proxy.packageManagers();
    int separateCount = 0;
    foreach(QString manager, managers) {
        QBENCHMARK {
            packages = _proxy.installedPackages(manager);
        }
        separateCount += packages.count();
    }
    QVERIFY(separateCount == allCount);
}


// tests that subsequent calls of installedPackages() is efficient
void test_PackageManagerProxy::installedPackagesPerf()
{
    // subsequent calls. This should not take long
    int MAX_ALLOWED = 200; // 1000 ms
    bool fast_enough = true;
    QTime time;
    time.start();

    QString package;
    QBENCHMARK {
        for( int i=0; i < _proxy.installedPackages().size(); ++i)
        {
            package = _proxy.installedPackages().at(i);
            if( time.elapsed() > MAX_ALLOWED) {
                fast_enough = false;
                break;
            }
        }
    }
    QVERIFY(fast_enough);
    QVERIFY(!package.isEmpty());
    QVERIFY(!_proxy.installedPackages().isEmpty());
}
