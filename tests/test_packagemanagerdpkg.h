/*
Copyright (c) 2014 Niko Kähkönen <niko.v.kahkonen@gmail.com>

This file is part of qt-packagemanager.

qt-packagemanager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _TEST_PACKAGEMANAGER_H_INCLUDED_
#define _TEST_PACKAGEMANAGER_H_INCLUDED_

#include <QObject>


class test_PackageManagerDpkg : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase();
    void cleanupTestCase();
    void listFiles();
    void listInstalled();
};

#endif // #ifndef _TEST_PACKAGEMANAGER_H_INCLUDED_

