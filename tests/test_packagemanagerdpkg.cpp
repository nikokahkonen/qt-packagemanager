/*
Copyright (c) 2014 Niko Kähkönen <niko.v.kahkonen@gmail.com>

This file is part of qt-packagemanager.

qt-packagemanager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QObject>
#include <QtTest/QtTest>
#include "test_packagemanagerdpkg.h"
#include "packagemanagerfactory.h"
#include "packagemanagerdpkg.h"



void test_PackageManagerDpkg::initTestCase()
{
}


void test_PackageManagerDpkg::cleanupTestCase()
{
}


void test_PackageManagerDpkg::listFiles()
{
    QStringList ret;
    QBENCHMARK {
        PackageManagerDpkg dpkg;
        ret = dpkg.installedPackages();
    }
    QVERIFY( ret.count() > 0);
}


void test_PackageManagerDpkg::listInstalled()
{
    QList<Package> pkgs;
    QBENCHMARK {
        pkgs = PackageManagerFactory::createManager("dpkg")->packages();
    }
}
